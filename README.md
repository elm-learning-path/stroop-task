# Stroop-task 

Une expérience de psychologie classique codée en Elm. 


## Pour lancer ce jeu 
 
 - https://gitlab.com/elm-learning-path/stroop-task.git
 - `cd stroop-task`
 - `npm install -g parcel`
 - `npm update`
 - `npm start`


## Mes apprentissages 

- Mise en place d'un projet
- Elm Architecture
- Utilisation des Cmds, Random, Time
- Premiers pas dans SVG, Elm-ui, html   


 ## Technologies
 
 Elm (elm-ui), parcel

 ## TODO:
- Envoyer les données vers airtable
- Plotter les données aggrégées dans l'app. 