module Main exposing (..)

import Browser exposing (element)
import Browser.Events exposing (..)
import Credentials exposing (myKey)
import Dict exposing (..)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import File.Download exposing (..)
import Html exposing (Html)
import Http exposing (..)
import Icons exposing (aperture)
import Json.Decode as Decode
import Json.Encode as Encode
import LineChart exposing (..)
import Process exposing (..)
import Random exposing (..)
import Task exposing (..)
import Time exposing (..)
import Tuple exposing (..)
import Parser exposing (Parser, (|.), (|=), succeed, symbol, spaces)


-- TODO : change process.sleep for onAnimationFrameDelta
-- TODO :  Change custom type's name to singular (SubjectActions -> SubjectAction)
-- TODO : NEttoyer tout c'est dégueu !
-- TODO : Convertir les données reçues et les mapper à Trial pour éviter de dupliquer les structures de données.
-- TODO : Generate Random List of Trials to improve perf

{--
███╗   ███╗ █████╗ ██╗███╗   ██╗
████╗ ████║██╔══██╗██║████╗  ██║
██╔████╔██║███████║██║██╔██╗ ██║
██║╚██╔╝██║██╔══██║██║██║╚██╗██║
██║ ╚═╝ ██║██║  ██║██║██║ ╚████║
╚═╝     ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝
--}


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- ███╗   ███╗ ██████╗ ██████╗ ███████╗██╗
-- ████╗ ████║██╔═══██╗██╔══██╗██╔════╝██║
-- ██╔████╔██║██║   ██║██║  ██║█████╗  ██║
-- ██║╚██╔╝██║██║   ██║██║  ██║██╔══╝  ██║
-- ██║ ╚═╝ ██║╚██████╔╝██████╔╝███████╗███████╗
-- ╚═╝     ╚═╝ ╚═════╝ ╚═════╝ ╚══════╝╚══════╝
--


type Model
    = Intro Experiment
    | ExperimentStarted Experiment
    | ExperimentEnded Experiment

type alias SessionInfo =
  { playerName : String
  , currentDate : String
  }

type alias Experiment =
    { instructions : String
    , experimentStatus : ExperimentStatus
    , trialId : Float
    , stimulus : ( Color, Word )
    , startedAt : Float
    , interruptedAt : Float
    , interruptedBy : SubjectAction
    , feedback : FeedBack
    , steps : Steps
    , maxTrial : Int
    , dataRecord : List Trial
    , numberOfGoodAnswers : Float
    , playerName : String
    , currentDate : String
    , remoteData : List TrialReceived
    , error : String
    }


type alias Trial =
    { trialId : Float
    , stimulus : ( Color, Word )
    , startedAt : Float
    , interruptedAt : Float
    , interruptedBy : SubjectAction
    }


type Records
    = Records (List Trial)


type Steps
    = FixationCrossVisible
    | GenerateNewTrial
    | GenerateNewPair
    | InputEvaluation
    | Introduction


type ExperimentStatus
    = Listening
    | NotListening


type FeedBack
    = Success
    | Failure
    | NoFeedback
    | FinalThankYou


type Request
    = RequestFailure
    | Loading
    | RequestSuccess String


type Word
    = BlueWord
    | RedWord
    | GreenWord


type Color
    = Blue
    | Red
    | Green
    | White
    | Black


type SubjectAction
    = RedColorSelected
    | BlueColorSelected
    | GreenColorSelected



-- ██╗███╗   ██╗██╗████████╗
-- ██║████╗  ██║██║╚══██╔══╝
-- ██║██╔██╗ ██║██║   ██║
-- ██║██║╚██╗██║██║   ██║
-- ██║██║ ╚████║██║   ██║
-- ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝
--

initIntro =
  { instructions = ""
  , experimentStatus = NotListening
  , trialId = 0
  , stimulus = ( White, BlueWord )
  , startedAt = 0
  , interruptedAt = 0
  , interruptedBy = RedColorSelected
  , feedback = Failure
  , steps = FixationCrossVisible
  , maxTrial = 10
  , dataRecord = []
  , numberOfGoodAnswers = 0
  , playerName = ""
  , currentDate = ""
  , remoteData = []
  , error = ""
  }

initExperimentEnded =
  { instructions = ""
  , experimentStatus = NotListening
  , trialId = 10
  , stimulus = ( White, BlueWord )
  , startedAt = 0
  , interruptedAt = 0
  , interruptedBy = RedColorSelected
  , feedback = Failure
  , steps = FixationCrossVisible
  , maxTrial = 10
  , dataRecord = [{ trialId = 10, startedAt = 1000, interruptedAt = 2000, interruptedBy = RedColorSelected, stimulus = (Blue, GreenWord)}]
  , numberOfGoodAnswers = 0
  , playerName = "test"
  , currentDate = "01-01-01"
  , remoteData = []
  , error = ""
  }


init : Flags -> ( Model, Cmd Msg )
init _ =
    ( Intro
        initIntro
    , Cmd.none
    )



-- ██╗   ██╗██████╗ ██████╗  █████╗ ████████╗███████╗
-- ██║   ██║██╔══██╗██╔══██╗██╔══██╗╚══██╔══╝██╔════╝
-- ██║   ██║██████╔╝██║  ██║███████║   ██║   █████╗
-- ██║   ██║██╔═══╝ ██║  ██║██╔══██║   ██║   ██╔══╝
-- ╚██████╔╝██║     ██████╔╝██║  ██║   ██║   ███████╗
--  ╚═════╝ ╚═╝     ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝
--


type Msg
    = NewPair ( Color, Word )
    | FixationCross ()
    | GotStimulusTime Time.Posix
    | GotInput SubjectAction
    | GotInputTime Time.Posix
    | InterTrialInterval
    | NewTrial ()
    | Update Experiment
    | GotAirtableAnswer (Result Http.Error String)
    | SaveData (List Trial)
    | GotData (Result Http.Error (List TrialReceived))
    | LoadingData
    | SendingData
    | DataSended (Result Http.Error String)



-- Wanted to turn a Msg into a Cmd Msg w/o side-effects.
-- Lesson : Not a good idea as it is inefficient and messes with the app's structure.
-- Alternative 1 : make a recursive call to update function.
-- Alternatve 2 : update directly in the branch.
-- source : https://medium.com/elm-shorts/how-to-turn-a-msg-into-a-cmd-msg-in-elm-5dd095175d84


experiment : Model -> Experiment
experiment moment =
    case moment of
        Intro model ->
          model

        ExperimentStarted model ->
           model

        ExperimentEnded model ->
          model



update : Msg -> Model -> ( Model, Cmd Msg )
update msg state =
    let
        model =
            experiment state
    in
    case msg of
        FixationCross () ->
            if model.trialId == toFloat model.maxTrial then
                ( ExperimentEnded { model | feedback = FinalThankYou }, Cmd.none )

            else
                ( ExperimentStarted
                    { model
                        | experimentStatus = NotListening
                        , steps = FixationCrossVisible
                        , feedback = NoFeedback
                    }
                , Process.sleep 1000 |> Task.perform NewTrial
                )

        NewTrial () ->
            ( ExperimentStarted
                { model
                    | experimentStatus = Listening
                    , steps = GenerateNewPair
                }
            , newPair
            )

        NewPair stimulus ->
            case stimulus of
                ( newWord, newColor ) ->
                    ( ExperimentStarted
                        { model
                            | stimulus = ( newWord, newColor )
                            , experimentStatus = Listening
                            , steps = GenerateNewPair
                            , feedback = NoFeedback
                        }
                    , getStimulusTime
                    )

        GotStimulusTime newStimTime ->
            ( ExperimentStarted
                { model
                    | startedAt = toFloat (posixToMillis newStimTime)
                }
            , Cmd.none
            )

        GotInput input ->
            let
                updateWith : FeedBack -> SubjectAction -> Int -> Model
                updateWith feedback subjectAction n =
                    ExperimentStarted
                        { model
                            | feedback = feedback
                            , interruptedBy = subjectAction
                            , numberOfGoodAnswers = model.numberOfGoodAnswers + toFloat n
                        }

                updateGiven : Color -> SubjectAction -> ( Model, Cmd Msg )
                updateGiven colorTarget subjectAction =
                    let
                        stimColor =
                            first model.stimulus
                    in
                    if stimColor == colorTarget then
                        ( updateWith Success subjectAction 1, getTime )

                    else
                        ( updateWith Failure subjectAction 0, getTime )
            in
            case input of
                RedColorSelected ->
                    updateGiven Red RedColorSelected

                BlueColorSelected ->
                    updateGiven Blue BlueColorSelected

                GreenColorSelected ->
                    updateGiven Green GreenColorSelected

        GotInputTime newTime ->
            ExperimentStarted
                { model
                    | interruptedAt = toFloat (posixToMillis newTime)
                }
                |> update InterTrialInterval

        InterTrialInterval ->
            ( ExperimentStarted
                { model
                    | stimulus = ( White, BlueWord )
                    , experimentStatus = NotListening
                    , dataRecord =
                        { trialId = model.trialId
                        , stimulus = model.stimulus
                        , startedAt = model.startedAt
                        , interruptedAt = model.interruptedAt
                        , interruptedBy = model.interruptedBy
                        }
                            :: model.dataRecord
                    , trialId = model.trialId + 1
                }
            , Process.sleep 2000 |> Task.perform FixationCross
            )

        Update new ->
            ( Intro new, Cmd.none )

        GotAirtableAnswer result ->
            Debug.todo ""

        SaveData data ->
            ( ExperimentEnded { model | feedback = FinalThankYou }, save data )

        GotData data ->
            case data of
              Ok newDataSet ->
                (ExperimentEnded { model | remoteData = newDataSet}, Cmd.none)
              Err errMessage ->
                (ExperimentEnded {model | error = Debug.toString errMessage}, Cmd.none)

        LoadingData ->
            ( ExperimentEnded model, getAirtableData )

        SendingData ->
            ( ExperimentEnded model, postAirtableData model.dataRecord )

        DataSended error ->
            case error of
                Ok message ->
                    ( ExperimentEnded { model | error = "C'est envoyé !" }, Cmd.none )

                Err errorMessage ->
                    (ExperimentEnded {model | error = Debug.toString errorMessage}, Cmd.none)

postAirtableData : List Trial -> Cmd Msg
postAirtableData data =
    Http.request
        { method = "POST"
        , headers = []
        , url = "https://api.airtable.com/v0/app1KrC2xpG9TvLv2/stroop-api?api_key=" ++ myKey
        , body = jsonBody (recordsEncoder data)
        , expect = Http.expectString DataSended
        , timeout = Just 3000
        , tracker = Nothing
        }
getAirtableData : Cmd Msg
getAirtableData =
    Http.get
        { url = "https://api.airtable.com/v0/app1KrC2xpG9TvLv2/stroop-api?api_key=" ++ myKey
        , expect = Http.expectJson GotData decoder
        }

trialDecoder =
    Decode.map5
        TrialReceived
        (Decode.field "trialId" Decode.float)
        (Decode.field "stimulus" Decode.string)
        (Decode.field "interruptedAt" Decode.float)
        (Decode.field "startedAt" Decode.float)
        (Decode.field "interruptedBy" Decode.string)
-- PAS BIEN DU TOUT, RAJOUTER UN DECODEUR DES QUE POSSIBLE
fieldsDecoder =
    Decode.field "fields" trialDecoder


decoder =
    Decode.field "records" (Decode.list fieldsDecoder)

type alias TrialReceived =
  { trialId : Float
  , stimulus : String
  , interruptedAt : Float
  , startedAt : Float
  , interruptedBy : String
  }

decodeColor colorString =
  case colorString of
    "Red" ->
      Task.succeed <| Red
    "Blue" ->
      Task.succeed <| Blue
    "Green" ->
      Task.succeed <| Green
    _ ->
      fail <| "I don't know a color named " ++ colorString

decodeWord wordString =
  case wordString of
    "Red" ->
      Task.succeed <| RedWord
    "Blue" ->
      Task.succeed <| BlueWord
    "GreenWord" ->
      Task.succeed <| GreenWord
    _ ->
      fail <| "I don't know that word " ++ wordString



recordsEncoder data =
    Encode.object [ ( "records", Encode.list fieldsEncoder data ) ]


fieldsEncoder data =
    Encode.object [ ( "fields", trialEncoder data ) ]


trialEncoder : Trial -> Encode.Value
trialEncoder data =
    Encode.object
        [ ( "trialId", Encode.float data.trialId )
        , ( "startedAt", Encode.float data.startedAt )
        , ( "stimulus", Encode.string (colorsToString (first data.stimulus) ++ (wordsToString (second data.stimulus)) ))
        , ( "interruptedAt", Encode.float data.interruptedAt )
        , ( "interruptedBy", Encode.string (subjectActionToString data.interruptedBy) )
        ]

getTime : Cmd Msg
getTime =
    Task.perform GotInputTime Time.now


getStimulusTime : Cmd Msg
getStimulusTime =
    Task.perform GotStimulusTime Time.now


pair : Random.Generator ( Color, Word )
pair =
    Random.pair
        (Random.uniform Blue [ Red, Green ])
        (Random.uniform BlueWord [ RedWord, GreenWord ])



newPair : Cmd Msg
newPair =
    Random.generate NewPair pair






-- ██╗   ██╗██╗███████╗██╗    ██╗
-- ██║   ██║██║██╔════╝██║    ██║
-- ██║   ██║██║█████╗  ██║ █╗ ██║
-- ╚██╗ ██╔╝██║██╔══╝  ██║███╗██║
--  ╚████╔╝ ██║███████╗╚███╔███╔╝
--   ╚═══╝  ╚═╝╚══════╝ ╚══╝╚══╝
--


view : Model -> Html Msg
view state =
    case state of
        Intro model ->
            Element.layout [] <|
                Element.column [ Element.width Element.fill, centerX ]
                    [ Element.row [ Element.width Element.fill, Background.color (colorsToRgb Blue) ]
                        [ Element.el [ Font.bold, Element.padding 30, Font.size 36, Font.color (colorsToRgb White) ] (Element.text "Stroop-task") ]
                    , Element.column [ centerX, centerY, padding 100, Element.spacing 100 ]
                        [ Input.username
                            []
                            { text = model.playerName
                            , placeholder = Just (Input.placeholder [] (text "ID of the player"))
                            , onChange = \new -> Update { model | playerName = new }
                            , label = Input.labelAbove [ Font.size 14 ] (text "Player of the ID")
                            }
                        , Input.text
                            []
                            { text = model.currentDate
                            , placeholder = Just (Input.placeholder [] (text "Date"))
                            , onChange = \new -> Update { model | currentDate = new }
                            , label = Input.labelAbove [ Font.size 14 ] (text "jj-mm-aaaa")
                            }
                        , el
                            [ centerX ]
                            (viewInstructions model.instructions)
                        , el
                            [ centerX ]
                            viewButtonStart
                        ]
                    ]

        ExperimentStarted model ->
            Element.layout [] <|
                Element.column [ centerX, centerY, padding 50, Element.spacing 50 ]
                    [ viewStimulus
                        (model.steps == FixationCrossVisible)
                        (wordsToString (second model.stimulus))
                        (colorsToRgb (first model.stimulus))
                    , el
                        [ centerX ]
                        (viewFeedback (feedBackToString model.feedback))
                    , el [ centerX ] (text (String.fromFloat model.trialId ++ "/" ++ String.fromInt model.maxTrial ++ " trials remaining"))
                    , el [ centerX ] (text (String.fromFloat (model.interruptedAt - model.startedAt) ++ " ms"))
                    ]

        ExperimentEnded model ->
            let
                recordsTable : List Trial
                recordsTable =
                    model.dataRecord
            in
            Element.layout [] <|
                Element.column [ centerX, centerY, padding 100, Element.spacing 100 ]
                    [ el
                        [ centerX ]
                      <|
                        Element.el
                            [ Font.color (colorsToRgb Black)
                            , Font.size 36
                            ]
                            (if model.numberOfGoodAnswers < 2 then
                                Element.text <|
                                    "Vous avez "
                                        ++ String.fromFloat model.numberOfGoodAnswers
                                        ++ " bonne réponse. Merci "
                                        ++ model.playerName
                                        ++ " !"

                             else
                                Element.text <|
                                    "Vous avez "
                                        ++ String.fromFloat model.numberOfGoodAnswers
                                        ++ " bonnes réponses. Merci "
                                        ++ model.playerName
                                        ++ " !"
                            )
                    , Element.table
                        [ Element.centerX
                        , Element.centerY
                        , Element.spacing 5
                        , Element.padding 10
                        ]
                        { data = recordsTable
                        , columns =
                            [ { header = Element.text "Trial ID"
                              , width = px 200
                              , view =
                                    \record ->
                                        Element.text (String.fromFloat record.trialId)
                              }
                            , { header = Element.text "startedAt"
                              , width = px 200
                              , view =
                                    \record ->
                                        Element.text (String.fromFloat record.startedAt)
                              }
                            , { header = Element.text "interruptedAt"
                              , width = px 200
                              , view =
                                    \record ->
                                        Element.text (String.fromFloat record.interruptedAt)
                              }
                            , { header = Element.text "Color Selected"
                              , width = px 200
                              , view =
                                    \record ->
                                        Element.text (actionToString record.interruptedBy)
                              }
                            ]
                        }
                    , Input.button
                        [ Background.color (colorsToRgb Blue)
                        , Font.color (colorsToRgb White)
                        , Border.color (colorsToRgb Black)
                        , Border.rounded 10
                        , padding 30
                        , centerX
                        ]
                        { onPress = Just (SaveData recordsTable)
                        , label = Element.text "Download as text"
                        }
                    , Input.button
                        [ Background.color (colorsToRgb Blue)
                        , Font.color (colorsToRgb White)
                        , Border.color (colorsToRgb Black)
                        , Border.rounded 10
                        , padding 30
                        , centerX
                        ]
                        { onPress = Just SendingData
                        , label = Element.text "Save Data to Database"
                        }
                    , Input.button
                        [ Background.color (colorsToRgb Blue)
                        , Font.color (colorsToRgb White)
                        , Border.color (colorsToRgb Black)
                        , Border.rounded 10
                        , padding 30
                        , centerX
                        ]
                        { onPress = Just LoadingData
                        , label = Element.text "Fetch Data from database"
                        }
                    , html (viewChart model.remoteData)
                    ]

type alias Point =
    { x : Float, y : Float }


viewChart : List TrialReceived -> Html msg
viewChart data =
    LineChart.view1 .trialId
        .interruptedAt
        data




save : List Trial -> Cmd msg
save data =
    File.Download.string "data.txt" "text/txt" (Debug.toString data)


actionToString : SubjectAction -> String
actionToString action =
    case action of
        RedColorSelected ->
            "red"

        BlueColorSelected ->
            "blue"

        GreenColorSelected ->
            "green"


viewHeader =
    Element.row [ Element.width Element.fill, Background.color (colorsToRgb Blue) ]


viewStimulus fixationCrossVisible word color =
    if fixationCrossVisible then
        Element.el
            [ Font.color (colorsToRgb Black)
            , Font.size 100
            , Font.family
                [ Font.typeface "Garamond"
                , Font.sansSerif
                ]
            , padding 100
            ]
            (Element.text "x")

    else
        Element.el
            [ Font.color color
            , Font.size 100
            , Font.family
                [ Font.typeface "Garamond"
                , Font.sansSerif
                ]
            , padding 100
            ]
            (Element.text word)


viewInstructions instruction =
    Element.el
        [ Font.color (colorsToRgb Black)
        , Font.size 25
        , Font.family
            [ Font.typeface "Helvetica"
            , Font.sansSerif
            ]
        ]
        (Element.text instruction)


viewFeedback feedback =
    Element.el
        [ Font.color (colorsToRgb Black)
        , Font.size 25
        , Font.family
            [ Font.typeface "Helvetica"
            , Font.sansSerif
            ]
        ]
        (Element.text feedback)


reactionTime : Trial -> Float
reactionTime trial =
    trial.interruptedAt - trial.startedAt


viewButtonStart =
    Input.button
        [ Background.color (colorsToRgb Blue)
        , Font.color (colorsToRgb White)
        , Border.color (colorsToRgb Black)
        , Border.rounded 10
        , padding 30
        ]
        { onPress = Just (FixationCross ())
        , label = Element.text "Start !"
        }





--Helpers to Convert my types.


feedBackToString : FeedBack -> String
feedBackToString feedback =
    case feedback of
        Success ->
            "Bravo 👌 !"

        Failure ->
            "Raté 😢 "

        NoFeedback ->
            ""

        _ ->
            ""


colorsToRgb : Color -> Element.Color
colorsToRgb color =
    case color of
        Blue ->
            rgb255 36 50 255

        Red ->
            rgb255 255 44 20

        Green ->
            rgb255 26 163 21

        White ->
            rgb255 255 255 255

        Black ->
            rgb255 0 0 0




type alias Stimulus =
  (Color, Word )
stimulusToString : Stimulus -> String
stimulusToString stim =
    case stim of
        ( a, b ) ->
            wordsToString b ++ colorsToString a


wordsToString : Word -> String
wordsToString word =
    case word of
        BlueWord ->
            "Blue"

        RedWord ->
            "Red"

        GreenWord ->
            "Green"


colorsToString : Color -> String
colorsToString color =
    case color of
        Blue ->
            "Blue"

        Red ->
            "Red"

        Green ->
            "Green"

        _ ->
            "Another Color"


subjectActionToString : SubjectAction -> String
subjectActionToString action =
    case action of
        RedColorSelected ->
            "RedColorSelected"

        GreenColorSelected ->
            "GreenColorSelected"

        BlueColorSelected ->
            "BlueColorSelected"


type alias Flags =
    ()



-- ███████╗██╗   ██╗██████╗ ███████╗ ██████╗██████╗ ██╗██████╗ ████████╗██╗ ██████╗ ███╗   ██╗███████╗
-- ██╔════╝██║   ██║██╔══██╗██╔════╝██╔════╝██╔══██╗██║██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║██╔════╝
-- ███████╗██║   ██║██████╔╝███████╗██║     ██████╔╝██║██████╔╝   ██║   ██║██║   ██║██╔██╗ ██║███████╗
-- ╚════██║██║   ██║██╔══██╗╚════██║██║     ██╔══██╗██║██╔═══╝    ██║   ██║██║   ██║██║╚██╗██║╚════██║
-- ███████║╚██████╔╝██████╔╝███████║╚██████╗██║  ██║██║██║        ██║   ██║╚██████╔╝██║ ╚████║███████║
-- ╚══════╝ ╚═════╝ ╚═════╝ ╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝╚═╝        ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚══════╝
--


subscriptions : Model -> Sub Msg
subscriptions state =
    let
        model =
            experiment state
    in
    case model.experimentStatus of
        Listening ->
            Sub.batch
                [ Browser.Events.onKeyDown (Decode.map GotInput keyDecoder) ]

        _ ->
            Sub.none


keyDecoder : Decode.Decoder SubjectAction
keyDecoder =
    Decode.field "key" Decode.string
        |> Decode.andThen keyToSubjectAction


keyToSubjectAction : String -> Decode.Decoder SubjectAction
keyToSubjectAction keyString =
    case keyString of
        "r" ->
            Decode.succeed RedColorSelected

        "g" ->
            Decode.succeed GreenColorSelected

        "b" ->
            Decode.succeed BlueColorSelected

        _ ->
            Decode.fail "not an event we care about"
